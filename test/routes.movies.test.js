const assert = require('assert');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const passport = require('passport');

const {moviesMock, MoviesServiceMock} = require('../utils/mocks/movies');
const testServer = require('../utils/testServer');

var stub = sinon.stub(passport, 'authenticate').returns(function() {});
stub.yields(new Error('fails here'));

describe('routes -movies', function () {
    const route = proxyquire('../routes/movies', {
        '../services/movies': MoviesServiceMock
    });

    const request = testServer(route);
    describe('GET /movies', function () {
        it('should respond with status 200', function (done) {
            request.get('/api/movies').expect(200, done);
        });

        it('should respond with the list of movies', function (done) {
            request.get('/api/movies').end((err, res) => {
                assert.deepEqual(res.body, {
                    data: moviesMock,
                    message: 'movies listed'
                });
            });

            done();
        })
    });
});

stub.restore();
