const MongoLib = require('../lib/mongo');

class UsersService {
    constructor() {
        this.collection = 'users';
        this.mongoDB = new MongoLib();
    }

    async get( userId ) {
        const user = await this.mongoDB.get(this.collection, userId);
        return user || undefined;
    }
}

module.exports = UsersService;