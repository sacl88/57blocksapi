const MongoLib = require('../lib/mongo');
const {ObjectId} = require('mongodb');
class MoviesService {
    constructor() {
        this.collection = 'movies';
        this.mongoDB = new MongoLib();
    }

    async getMovies({type}, {user}, {page, show}, {with_my_likes}) {  
        const filterPrivate = type && (type.toLowerCase() === 'private');
        let query = { private: {$in: [null, false]} }

        if (typeof with_my_likes !== 'undefined') {
            query = { private: {$in: [null, false]}, $and: [{"likes._id": user._id} ] };
        } else {
            if (type === 'public') {
                query = type && { private: {$in: [null, filterPrivate]} };
            }
    
            if (type === 'private') {
                query = type && { private: {$in: [null, filterPrivate]}, "author" : ObjectId(user._id).toString() };
            } 
        }               
        
        const movies = await this.mongoDB.getAll(this.collection, query, {page, show});
        return movies || [];
    }

    async getMovie({ movieId }) {
        const movie = await this.mongoDB.get(this.collection, movieId);
        return movie || {};
    }

    async createMovie({ movie }) {
        const createMovieId = await this.mongoDB.create(this.collection, movie);
        return createMovieId || {};
    }

    async updateMovie({ movieId, movie } = {}) {
        const updatedMovieId = await this.mongoDB.update(this.collection, movieId, movie);
        return updatedMovieId || {};
    }

    async saveLike({ movieId, user }) {
        const saveLikeToMovie = await this.mongoDB.saveLike(this.collection, movieId, user);
        return saveLikeToMovie || {};
    }

    async deleteMovie({ movieId }) {
        const deletedMovieId = await this.mongoDB.delete(this.collection, movieId);
        return deletedMovieId || {};
    }
}

module.exports = MoviesService;