const MongoLib = require('../lib/mongo');

class AuthService {
    constructor() {
        this.collection = 'users';
        this.mongoDB = new MongoLib();
    }

    async getUserByEmail({ email }) {
        const query = { email: email };
        const user = await this.mongoDB.getByTerm(this.collection, query);
        return user || undefined;
    }

    async register(dataUser) {
        const user = await this.mongoDB.register(this.collection, dataUser);
        return user || {};
    }

    async login(dataUser) {
        const user = await this.mongoDB.login(this.collection, dataUser);
        return user || undefined;
    }
}

module.exports = AuthService;