const express = require('express');
const MovieService = require('../services/movies');
const {ObjectId} = require('mongodb');

const {
    movieIdSchema,
    createMovieSchema,
    updateMovieSchema
} = require('../utils/schemas/movies');

const validationHandler = require('../utils/middleware/validationHandler');

const cacheResponse = require('../utils/cacheResponse');
const { FIVE_MINUTES_IN_SECONDS, SIXTY_MINUTES_IN_SECONDS } = require('../utils/time');

const moviesService = new MovieService();

const setAuthor = function (req, res, next) {
    req.body.author = ObjectId(req.user._id).toString();
    next();
};

const updatePolicy = async function (req, res, next) {
    const {movieId} = req.params;
    const movie = await moviesService.getMovie({movieId});
    if (movie.author !== ObjectId(req.user._id).toString()) {
        res.status(401).json({
            message: 'Unauthorized'
        });
    } else {
        next();
    }    
}

function moviesApi(app, passport) {
    const router = express.Router();
    app.use("/api/movies", router);

    router.get("/", passport.authenticate('jwt', {session:false}), async function (req, res, next) {
        cacheResponse(res, FIVE_MINUTES_IN_SECONDS);

        const { page = 1, show = 0 } = req.query;

        const { type } = req.query;   
        const { with_my_likes } = req.query;      
        const { user } = req;
        try {
            const movies = await moviesService.getMovies({type}, {user}, {page, show}, {with_my_likes});

            res.status(200).json({
                data: movies,
                message: 'movies listed'
            });
        } catch (e) {
            next(e);
        }
    });

    router.get("/:movieId", 
        passport.authenticate('jwt', {session:false}), 
        validationHandler({movieId: movieIdSchema}, 'params'), 
        async function (req, res, next) {

            cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);

            const { movieId } = req.params;
            try {
                const movie = await moviesService.getMovie({movieId});

                res.status(200).json({
                    data: movie,
                    message: 'movie retrieve'
                });
            } catch (e) {
                next(e);
            }
    });

    router.post("/", 
        passport.authenticate('jwt', {session:false}), 
        setAuthor, 
        validationHandler(createMovieSchema), 
        async function (req, res, next) {
            const { body: movie } = req;
            try {
                const createdMovieId = await moviesService.createMovie({ movie });

                res.status(201).json({
                    data: createdMovieId,
                    message: 'movie created'
                })
            } catch (e) {
                next(e);
            }
    });

    router.put("/:movieId", 
        passport.authenticate('jwt', {session:false}), 
        updatePolicy, 
        validationHandler({movieId: movieIdSchema}, 'params'), 
        validationHandler(updateMovieSchema), 
        async function (req, res, next) {
            const { movieId } = req.params;
            const { body: movie } = req;
            try {
                const updatedMovieId = await moviesService.updateMovie({ movieId, movie });

                res.status(200).json({
                    data: updatedMovieId,
                    message: 'movie updated'
                })
            } catch (e) {
                next(e);
            }
    });

    router.put("/like/:movieId", 
        passport.authenticate('jwt', {session:false}), 
        validationHandler({movieId: movieIdSchema}, 'params'), 
        async function (req, res, next) {
            const { movieId } = req.params;
            const { user: user } = req;
            try {
                const saveLikeToMovie = await moviesService.saveLike({ movieId, user });

                res.status(200).json({
                    data: saveLikeToMovie,
                    message: 'I like saved'
                })
            } catch (e) {
                next(e);
            }
    });

    router.delete("/:movieId", 
        passport.authenticate('jwt', {session:false}), 
        updatePolicy, 
        validationHandler({movieId: movieIdSchema}, 'params'), 
        async function (req, res, next) {
            const { movieId } = req.params;
            try {
                const deletedMovie = await moviesService.deleteMovie({ movieId });

                res.status(200).json({
                    data: deletedMovie,
                    message: 'movie deleted'
                })
            } catch (e) {
                next(e);
            }
    });
}

module.exports = moviesApi;