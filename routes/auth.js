const express = require('express');
var jwt = require('jsonwebtoken');
const securePassword = require("./../utils/securePassword");
const AuthService = require('../services/auth');

const {
    registerUserSchema,
    loginUserSchema
} = require('../utils/schemas/auth');

const validationHandler = require('../utils/middleware/validationHandler');

function authApi(app, passport) {
    const router = express.Router();
    app.use('/api', router);

    const authService = new AuthService();

    router.post('/register', validationHandler(registerUserSchema), async function (req, res, next) {
        try {
            const email = req.body.email;
            const existingUser = await authService.getUserByEmail({email});
            if (existingUser) {
                res.status(403);
                return res.json("User Already Exists");
            }

            const dataUser = {
				email:req.body.email,
				password:req.body.password
			}

            const user = await authService.register(dataUser)

            const token = jwt.sign({_id: user.insertedId}, 'secret', {
                expiresIn: 1200 // in seconds
            });

            res.status(200)
				.json({
                    success:true, 
                    message: 'Successfully created new user.', 
                    token: `JWT ${token}`, 
                    user: user
                });
                
        } catch (e) {
            next(e);
        }
    })

    router.post('/login', validationHandler(loginUserSchema), async function (req, res, next) {
        const dataUser = {
            email:req.body.email,
            password:req.body.password
        }

        const user = await authService.login(dataUser);

        if (!user) {
            res.status(403);
                return res.json("Authentication failed, User not found.");
        }

        const token = jwt.sign({_id: user._id, email: user.email}, 'secret', {
            expiresIn: 1200 // in seconds
        });

        res.status(200)
            .json({
                success:true, 
                message: 'Authentication Successfully', 
                token: `JWT ${token}`, 
                user: {
                    _id: user._id,
                    email: user.email
                }
            });
    })
}

module.exports = authApi;