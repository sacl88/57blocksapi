const express = require('express');
const passport = require('passport');
const supertest = require('supertest');
const sinon = require('sinon');

function testServer(route) {
    const app = express();
    require('../config/passport')(passport);
    app.use(passport.initialize());
        
    route(app, passport);
    return supertest(app, passport);
}

module.exports = testServer;