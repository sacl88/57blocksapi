const joi = require('@hapi/joi');

const passwordSchema = joi
                        .string()
                        .min(10)
                        .trim(true)
                        .pattern(new RegExp('^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@!#\\]\\?]).*$'));
const emailSchema = joi.string().email();

const registerUserSchema = {
    email: emailSchema.required(),
    password: passwordSchema.required().messages({
        "string.pattern.base": `The "password" must have a minimum of 10 characters, it must have a lowercase, an uppercase and at least one of the following symbols "@, !, #, ?, or ]"`,
    })    
};

const passwordLoginSchema = joi.string();
const emailLoginSchema = joi.string().email();

const loginUserSchema = {
    email: emailLoginSchema.required(),
    password: passwordLoginSchema.required()  
}

module.exports = {
    registerUserSchema,
    loginUserSchema
};