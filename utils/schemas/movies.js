const joi = require('@hapi/joi');

const movieIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const movieTitleSxchema = joi.string().max(80);
const movieYearSchema = joi.number().min(1888).max(2077);
const movieCoverSchema = joi.string().uri();
const movieDescriiptionSchema = joi.string().max(300);
const movieDurationSchema = joi.number().min(1).max(300);
const movieContentRaitingSchema = joi.string().max(5);
const movieSourceSchema = joi.string().uri();
const movieTagsSchema = joi.array().items(joi.string().max(50));
const moviePrivateItem = joi.bool();
const movieAuthor = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const createMovieSchema = {
    title: movieTitleSxchema.required(),
    year: movieYearSchema.required(),
    cover: movieCoverSchema.required(),
    description: movieDescriiptionSchema.required(),
    duration: movieDurationSchema.required(),
    contentRaiting: movieContentRaitingSchema.required(),
    source: movieSourceSchema.required(),
    tags: movieTagsSchema,
    author: movieAuthor.required(),    
    private: moviePrivateItem.required().messages({
        "bool": `The "private" You must mark if the article is private or public.`,
    }) 
};

const updateMovieSchema = {
    title: movieTitleSxchema,
    year: movieYearSchema,
    cover: movieCoverSchema,
    description: movieDescriiptionSchema,
    duration: movieDurationSchema,
    contentRaiting: movieContentRaitingSchema,
    source: movieSourceSchema,
    tags: movieTagsSchema,
    private: moviePrivateItem
};

module.exports = {
    movieIdSchema,
    createMovieSchema,
    updateMovieSchema
};