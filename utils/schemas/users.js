const joi = require('@hapi/joi');

const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const userEmailSchema = joi.string().email();

const userSchema = {
    _id: userIdSchema.required(),
    email: userEmailSchema.required(),
}

module.exports = {
    userSchema
};