const express = require('express');
const app = express();
const debug = require('debug')('app:server');

const {config} = require('./config/index');
const authApi = require('./routes/auth');
const moviesApi = require('./routes/movies');
const passport = require('passport');
require('./config/passport')(passport);

const {
    logErrors,
    wrapErrors,
    errorHandler
} = require('./utils/middleware/errorHandlers');

const notFoundHandler = require('./utils/middleware/notFoundHandler');

// body parser
app.use(express.json());
app.use(passport.initialize());

authApi(app, passport);
moviesApi(app, passport);

// Catch 404
app.use(notFoundHandler);

// Errors middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

app.listen(config.port, function () {
    debug(`Listening http://127.0.0.1:${config.port}`);
});