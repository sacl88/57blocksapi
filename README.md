execute commans

    npm install
    
    npm run dev 
## Create file .env with this example and user test Mongo
    // CONFIG
    PORT=3000
    CORS=*

    // MONGO
    DB_USER=sacl88
    DB_PASSWORD=QBj6QsLzsBOSK4Lr
    DB_HOST=movies.o7y3m.mongodb.net
    DB_NAME=movies

## Request & Response Examples

### API Resources
  - [POST /api/register](#post-apiregister)
  - [POST /api/login](#post-apilogin)
  - [GET /api/movies](#get-apimovies)
  - [POST /api/movies](#post-movies)
  - [GET /api/movies/[id]](#get-apimoviesid)
  - [PUT /api/movies/[id]](#put-apimoviesid)
  - [PUT /api/movies/like/[id]](#put-apimovieslikeid)  
  - [GET /api/movies/[params]](#get-apimoviesparams)
  - [DELETE /api/movies/[id]](#post-apimoviesid)

### POST /api/register

Example: http://127.0.0.1:3000/api/register

request: 
   
    {
        "email": "sergio6@gmail.com",
        "password": "1234567890Ae]"
    }

Response success: 

    {
        "success": true,
        "message": "Successfully created new user.",
        "token": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MThiOGE2Mjk0MjJmMGQxMGQ2YzMyM2MiLCJpYXQiOjE2MzY1MzQ4ODIsImV4cCI6MTYzNjUzNjA4Mn0.J4wJzuBjFXr9HfXvWz1dInf2hSN8TWT7US3DLSqfcbk",
        "user": {
            "acknowledged": true,
            "insertedId": "618b8a629422f0d10d6c323c"
        }
    }

Response error: "User Already Exists" or validations messages

### POST /api/login

Example: http://127.0.0.1:3000/api/login

request: 

    {
        "email": "sergio6@gmail.com",
        "password": "1234567890Ae]"
    }

Response success: 

    {
        "success": true,
        "message": "Authentication Successfully",
        "token": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MThiOGE2Mjk0MjJmMGQxMGQ2YzMyM2MiLCJlbWFpbCI6InNlcmdpbzdAZ21haWwuY29tIiwiaWF0IjoxNjM2NTM1MjE3LCJleHAiOjE2MzY1MzY0MTd9.YZ2FOIQuBFKimzH6AdmQfUqMeVOQ06MhKheOpsz0Wgw",
        "user": {
            "_id": "618b8a629422f0d10d6c323c",
            "email": "sergio7@gmail.com"
        }
    }

### GET /api/movies

Example: http://127.0.0.1:3000/api/movies

Example with pagination: http://127.0.0.1:3000/api/movies?page=1&show=15

Response body with public items:

    {
        "data": [
            {
                "_id": "618b4eb8c0dbdc85e7cc12b1",
                "title": "Terminator 3",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": false,
                "author": "618abafdf497fe235eaf7fca",
                "likes": [
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    },
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    }
                ]
            },
            {
                "_id": "618b4ebec0dbdc85e7cc12b2",
                "title": "Terminator 4",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": false,
                "author": "618abafdf497fe235eaf7fca",
                "likes": [
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    }
                ]
            },
            {
                "_id": "618b4ec2c0dbdc85e7cc12b3",
                "title": "Terminator 5",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": false,
                "author": "618abafdf497fe235eaf7fca"
            }
        ],
        "message": "movies listed"
    }

### POST /api/movies

Example: http://127.0.0.1:3000/api/movies

Note: to apply as private or public, you just need to change the private property type boolean

Request :

    {
        "title": "Gestapo's Last Orgy, The (L'ultima orgia del III Reich)",
        "year": 2012,
        "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
        "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
        "duration": 3,
        "contentRaiting": "NC-17",
        "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
        "tags": [
            "Drama",
            "Children|Comedy|Fantasy",
            "Horror|Sci-Fi|Thriller",
            "Crime|Drama|Film-Noir|Thriller"
        ],
        "private": true
    }


Response:   {
                "data": "618b8db19422f0d10d6c323d",
                "message": "movie created"
            }

### PUT /api/movies/[id]
Example: http://127.0.0.1:3000/api/movies/618b8db19422f0d10d6c323d

Request :

    {
        "title": "Gestapo's Last Orgy, The (L'ultima orgia del III Reich)",
        "year": 2012,
        "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
        "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
        "duration": 3,
        "contentRaiting": "NC-17",
        "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
        "tags": [
            "Drama",
            "Children|Comedy|Fantasy",
            "Horror|Sci-Fi|Thriller",
            "Crime|Drama|Film-Noir|Thriller"
        ],
        "private": true
    }

Response :  {
                "data": "618b8db19422f0d10d6c323d",
                "message": "movie updated"
            }

### DELETE /api/movies/[id]
Example: http://127.0.0.1:3000/api/movies/618b8db19422f0d10d6c323d

Response: 

    {
        "data": "618b8db19422f0d10d6c323d",
        "message": "movie deleted"
    }

### PUT /api/movies/like/[id]

Example: http://127.0.0.1:3000/api/movies/like/618b4eb8c0dbdc85e7cc12b1

Response :  

    {
        "data": "618b4eb8c0dbdc85e7cc12b1",
        "message": "I like saved"
    }

### GET /api/movies/[params]

## Search movies with my likes
Example : http://127.0.0.1:3000/api/movies?page=1&show=1&with_my_likes

Response : 

    {
        "data": [
            {
                "_id": "618b4eb8c0dbdc85e7cc12b1",
                "title": "Terminator 3",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": false,
                "author": "618abafdf497fe235eaf7fca",
                "likes": [
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    }
                ]
            }
        ],
        "message": "movies listed"
    }

## Search movies my private
Example : http://127.0.0.1:3000/api/movies?page=1&show=1&type=private

Response : 

    {
        "data": [
            {
                "_id": "618b4eb8c0dbdc85e7cc12b1",
                "title": "Terminator 3",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": true,
                "author": "618abafdf497fe235eaf7fca",
                "likes": [
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    }
                ]
            }
        ],
        "message": "movies listed"
    }


## Search movies public
Example : http://127.0.0.1:3000/api/movies?page=1&show=1&type=public

Response : 

    {
        "data": [
            {
                "_id": "618b4eb8c0dbdc85e7cc12b1",
                "title": "Terminator 3",
                "year": 2012,
                "cover": "http://dummyimage.com/249x100.jpg/cc0000/ffffff",
                "description": "Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.",
                "duration": 3,
                "contentRaiting": "NC-17",
                "source": "https://ovh.net/semper/rutrum/nulla/nunc.jsp",
                "tags": [
                    "Fantasyr"
                ],
                "private": false,
                "author": "618abafdf497fe235eaf7fca",
                "likes": [
                    {
                        "_id": "618abafdf497fe235eaf7fca"
                    }
                ]
            }
        ],
        "message": "movies listed"
    }

## run tests
    npm run test