const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const UsersService = require('../services/users');
module.exports = function(passport) {
	var opts = {};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
	opts.secretOrKey = 'secret';
	opts.algorithms = ['HS256']

	passport.use(new JwtStrategy(opts, async function(jwt_payload, done) {
		const usersService = new UsersService();
		const _id = jwt_payload._id;
		const user = await usersService.get(_id);
		if (user) {
			done(null, {
				_id: user._id,
				email: user.email
			});
		} else {
			done(null, false);
		}
	}))
}